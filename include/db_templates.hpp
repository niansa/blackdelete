#pragma once

namespace db_templates {
    enum type {
        channel,
        _end [[maybe_unused]], // Must be last one before _default
        _default = channel // Must be last one
    };
}
