#include <string>
#include <unordered_map>
#include <tuple>
#include <chrono>
#include <exception>
#include <algorithm>
#include <boost/asio.hpp>
#include "cdltypes.hpp"
#include "database.hpp"

using namespace CDL;
using namespace std;
using namespace chrono_literals;

vector<const_CMessage> running_delays;

bool running_delays_remove(const_CMessage msg) {
    auto res = find(running_delays.begin(), running_delays.end(), msg);
    if (res != running_delays.end()) {
        running_delays.erase(res);
        return true;
    } else {
        return false;
    }
}


void set_delay(CMessage msg, CChannel channel, cmdargs& args) {
    // Check permissions
    if (!msg->member->has_perm(Permissions::ADMINISTRATOR)) {
        channel->send("You need to be an administrator to perform this operation");
        return;
    }
    // Check args
    if (args.empty()) {
        channel->send("Usage: "+msg->content+" <delay in seconds (0 for none)>");
        return;
    }
    // Get number
    int delay;
    try {
        delay = stoi(args[0]);
    } catch (exception& e) {
        channel->send("Invalid number (Exception: "+std::string(e.what())+")");
        return;
    }
    // Verify number
    if (delay < 0) {
        channel->send("Invalid number (May not be negative)");
        return;
    }
    constexpr int maxdelay = chrono::duration_cast<chrono::seconds>(6h).count();
    if (delay > maxdelay) {
        channel->send("Delays higher than "+to_string(maxdelay)+" seconds are not allowed!");
        return;
    }
    // Perform database operation
    env.db->update(to_dbid(channel->id), "DELAY", delay, [=] (const bool error) {
        if (error) {
            channel->send("Database operation has failed");
        } else {
            channel->send("New deletion delay for this channel: "+to_string(delay)+'s');
        }
    });
}

void get_delay(CMessage, CChannel channel, cmdargs&) {
    env.db->get<int>(to_dbid(channel->id), "DELAY", [=] (auto delay) {
        channel->send("Deletion delay for this channel: "+(delay?to_string(delay)+'s':"none"));
    });
}

void delay_message(CMessage msg) {
    env.db->get<int>(to_dbid(msg->channel_id), "DELAY", [=] (auto delay) {
        if (delay) {
            running_delays.push_back(msg);
            auto timer = new boost::asio::deadline_timer(*env.aioc, boost::posix_time::seconds(delay));
            timer->async_wait([=] (...) {
                delete timer;
                if (running_delays_remove(msg)) {
                    msg->remove();
                }
            });
        }
    });
}

void undelay_message(const_CMessage msg) {
    running_delays_remove(msg);
}



static const vector<string> get_table_tmpl(db_templates::type type) {
    using namespace db_templates;
    switch (type) {
        case channel:
            return {"DELAY INT"};
    }
    return {};
}
static const map<string, string> get_record_tmpl(db_templates::type type) {
    using namespace db_templates;
    switch (type) {
        case channel:
            return {
                       {"DELAY", "0"}
                   };
    }
    return {};
}

int main(int argc, char **argv) {
    register_command("set_delay", set_delay, 1);
    register_command("get_delay", get_delay, NO_ARGS);

    handlers::get_prefix = [] (CChannel, auto cb) {
        cb("bd!");
    };

    handlers::in_main = [] () {
        env.db = new Database(get_table_tmpl, get_record_tmpl);
    };

    intents::message_create.push_back(delay_message);
    intents::message_delete.push_back(undelay_message);
    intents::channel_pins_update.push_back([] (CChannel channel) {
        channel->get_pins([] (const bool error, const unordered_map<uint64_t, CMessage>& pins) {
            if (!error) {
                for (const auto& [msg_id, msg] : pins) {
                    undelay_message(msg);
                }
            }
        });
    });

    using namespace intent_vals;
    CDL::main(argc, argv, GUILD_MESSAGES);
}
